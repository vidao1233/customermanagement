﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CustomerManagement.Data;
using CustomerManagement.Models;

namespace CustomerManagement.Controllers
{
    public class BranchesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BranchesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Branches
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var _Branch = await _context.Branches.ToListAsync();
            if (_Branch.Count < 1)
                await CreateTestData();

            return _context.Branches != null ?
                        View(await _context.Branches.ToListAsync()) :
                        Problem("Entity set 'ApplicationDbContext.Branch'  is null.");
        }

        // GET: Branches/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Branches == null)
            {
                return NotFound();
            }

            var branch = await _context.Branches
                .FirstOrDefaultAsync(m => m.BranchId == id);
            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        // GET: Branches/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Branches == null)
            {
                return NotFound();
            }

            var branch = await _context.Branches.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }
            return View(branch);
        }

        // PUT: Branches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BranchId,Name,Address,City,State,Zipcode")] Branch branch)
        {
            if (id != branch.BranchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(branch);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BranchExists(branch.BranchId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(branch);
        }

        // GET: Branches/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Branches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BranchId,Name,Address,City,State,Zipcode")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                _context.Add(branch);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(branch);
        }

        // DELETE: Branches/5
        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            try
            {
                var _Branches = await _context.Branches.FindAsync(id);

                if (_Branches != null)
                {
                    _context.Branches.Remove(_Branches);
                }
                await _context.SaveChangesAsync();
                return new JsonResult(_Branches);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool BranchExists(int id)
        {
            return (_context.Branches?.Any(e => e.BranchId == id)).GetValueOrDefault();
        }
        private async Task CreateTestData()
        {
            foreach (var item in GetBranchList())
            {
                _context.Branches.Add(item);
                await _context.SaveChangesAsync();
            }
        }

        private IEnumerable<Branch> GetBranchList()
        {
            return new List<Branch>
            {
                new Branch {Name = "Branch 1", Address = "Nguy Nhu - Kon Tum", City = "Ha Noi", State = "Active", Zipcode = "001"},
                new Branch {Name = "Branch 2", Address = "1 - Vo Van Ngan", City = "Ho Chi Minh", State = "Active", Zipcode = "002"},
                new Branch {Name = "Branch 3", Address = "1 - Le Van Viet", City = "Ho Chi Minh", State = "Active", Zipcode = "003"},
                new Branch {Name = "Branch 4", Address = "1 - Le Van Chi", City = "Ho Chi Minh", State = "DeActive", Zipcode = "004"},
                new Branch {Name = "Branch 5", Address = "1 - Dinh Phong Phu", City = "Ho Chi Minh", State = "Active", Zipcode = "005"},
            };
        }
    }
}
